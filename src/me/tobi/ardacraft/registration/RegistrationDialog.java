package me.tobi.ardacraft.registration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.User;
import me.tobi.ardacraft.api.message.FancyMessage;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class RegistrationDialog {
	
	Player p;
	
	public RegistrationDialog(Player player) {
		p = player;
	}
	
	public void showCharacter(String nick) {
		Charakter c = ArdaCraftAPI.getACDatabase().getCharacterManager().get(nick);
		p.sendMessage("§a===============Charakter===============");
		p.sendMessage("Character Name: " + c.getName());
		p.sendMessage("Volk:           " + c.getRasse());
		String taken = User.getByName(nick).getCharakter()==null?"§aNein":"§cJa";
		p.sendMessage("In Verwendung:  " + taken);
		String main = c.isMain()==null?"§aNein":"§cJa";
		p.sendMessage("Hauptcharakter: " + main);
		p.sendMessage("§a=======================================");
	}
	
	public void showCharacters(Rasse rasse, boolean maincharactersClickable) {
		final List<Charakter> charaktere = ArdaCraftAPI.getACDatabase().getCharacterManager().get(rasse);
		Random rnd = new Random();
		List<Charakter> characters = new ArrayList<Charakter>();
		for(int i = 0; i<12; i++) {
			int n = rnd.nextInt(charaktere.size());
			Charakter c = charaktere.get(n);
			int count = 0;
			while(characters.contains(c)) {
				if(count > 200) {
					break;
				}
				c = charaktere.get(rnd.nextInt(charaktere.size()));
				count ++;
			}
			if(!(characters.contains(c))) {
				characters.add(c);
			}
		}
		FancyMessage msg = new FancyMessage("§eCharaktere: ");
		for(Charakter c : characters) {
			if((c.isMain() && !maincharactersClickable)) {
				//System.out.println("main: " + c.isMain() + "; mainCLickable: " + maincharactersClickable);
				String name = "§c" + c.getName() + ", ";
				msg.then(name).tooltip("§cHauptcharakter").command("/character set " + p.getName() + " " + c.getName() + " false false");
			}else if(c.isTaken()) {
				//System.out.println("taken: " + c.isTaken());
				String name = "§c" + c.getName() + ", ";
				msg.then(name).tooltip("§cBereits Verwendet").command("/character set " + p.getName() + " " + c.getName() + " false false");
			}else {
			
				String name = "§a" + c.getName() + ", ";
				msg.then(name).tooltip("§aFrei").command("/character set " + p.getName() + " " + c.getName() + " false false");
			}
			
		}
		p.sendMessage("§a==============Charaktere==============");
		p.sendMessage("§bKlicke den Charakter an, den du spielen willst. Rote Charaktere kannst du nicht spielen!");
		//for(Player p2 : Bukkit.getServer().getOnlinePlayers()){
			IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(msg.toJSONString());
			PacketPlayOutChat packet = new PacketPlayOutChat(comp);
	        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
		//}
		p.sendMessage("§a======================================");
		
		
	}
	
}
