package me.tobi.ardacraft.registration;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Rasse;

import java.util.ArrayList;
import java.util.List;

public class Data {
	
	public static List<Charakter> charaktere = new ArrayList<Charakter>();
	
	public static void writeToDatabase(){

        Charakter adrahil = new Charakter(0, "Adrahil", Rasse.MENSCH, false);
        charaktere.add(adrahil);

        Charakter aldamir = new Charakter(1, "Aldamir", Rasse.MENSCH, false);
        charaktere.add(aldamir);

        Charakter anardil = new Charakter(2, "Anardil", Rasse.MENSCH, false);
        charaktere.add(anardil);

        Charakter angborn = new Charakter (3, "Angborn", Rasse.MENSCH, false);
        charaktere.add(angborn);

        Charakter angbor = new Charakter(4, "Angbor", Rasse.MENSCH, false);
        charaktere.add(angbor);

        Charakter angelimir = new Charakter(5, "Angelimrir", Rasse.MENSCH, false);
        charaktere.add(angelimir);

        Charakter aratan = new Charakter(6, "Aratan", Rasse.MENSCH, false);
        charaktere.add(aratan);

        Charakter arciryas = new Charakter(7, "Arciryas", Rasse.MENSCH, false);
        charaktere.add(arciryas);

        Charakter artamir = new Charakter(8, "Artamir", Rasse.MENSCH, false);
        charaktere.add(artamir);

        Charakter atanatar = new Charakter(9, "Atanatar", Rasse.MENSCH, false);
        charaktere.add(atanatar);

        Charakter barahir = new Charakter(10, "Barahir", Rasse.MENSCH, false);
        charaktere.add(barahir);

        Charakter baranor = new Charakter(11, "Baranor", Rasse.MENSCH, false);
        charaktere.add(baranor);

        Charakter belecthor = new Charakter(12, "Belecthor", Rasse.MENSCH, false);
        charaktere.add(belecthor);

        Charakter belegorn = new Charakter(13, "Belegorn", Rasse.MENSCH, false);
        charaktere.add(belegorn);

        Charakter beregond = new Charakter(14, "Beregond", Rasse.MENSCH, false);
        charaktere.add(beregond);

        Charakter beren = new Charakter(15, "Beren", Rasse.MENSCH, false);
        charaktere.add(beren);

        Charakter bergil = new Charakter(16, "Bergil", Rasse.MENSCH, false);
        charaktere.add(bergil);

        Charakter beruthiel = new Charakter(17, "Beruthiel", Rasse.MENSCH, false);
        charaktere.add(beruthiel);

        Charakter boromir = new Charakter(18, "Boromir", Rasse.MENSCH, true);
        charaktere.add(boromir);

        Charakter calimehtar = new Charakter(19, "Calimehtar", Rasse.MENSCH, false);
        charaktere.add(calimehtar);

        Charakter calimmacil = new Charakter(20, "Calimmacil", Rasse.MENSCH, false);
        charaktere.add(calimmacil);

        Charakter calmacil = new Charakter(21, "Calmacil", Rasse.MENSCH, false);
        charaktere.add(calmacil);

        Charakter castamir = new Charakter(22, "Castamir", Rasse.MENSCH, false);
        charaktere.add(castamir);

        Charakter cemendur = new Charakter(23, "Cemendur", Rasse.MENSCH, false);
        charaktere.add(cemendur);

        Charakter cirion = new Charakter(24, "Cirion", Rasse.MENSCH, false);
        charaktere.add(cirion);

        Charakter ciryandil = new Charakter(25, "Ciryandil", Rasse.MENSCH, false);
        charaktere.add(ciryandil);

        Charakter ciryon = new Charakter(26, "Ciryon", Rasse.MENSCH, false);
        charaktere.add(ciryon);

        Charakter damrod = new Charakter(27, "Damrod", Rasse.MENSCH, false);
        charaktere.add(damrod);

        Charakter denethor = new Charakter(28, "Denethor", Rasse.MENSCH, true);
        charaktere.add(denethor);

        Charakter derufin = new Charakter(29, "Derufin", Rasse.MENSCH, false);
        charaktere.add(derufin);

        Charakter dervorin = new Charakter(30, "Dervorin", Rasse.MENSCH, false);
        charaktere.add(dervorin);

        Charakter duilin = new Charakter(31, "Duilin", Rasse.MENSCH, false);
        charaktere.add(duilin);

        Charakter duinhir = new Charakter(32, "Duinhir", Rasse.MENSCH, false);
        charaktere.add(duinhir);

        Charakter earnil = new Charakter(33, "Earnil", Rasse.MENSCH, false);
        charaktere.add(earnil);

        Charakter earnur = new Charakter(34, "Earnur", Rasse.MENSCH, false);
        charaktere.add(earnur);

        Charakter egalmoth = new Charakter(35, "Egalmoth", Rasse.MENSCH, false);
        charaktere.add(egalmoth);

        Charakter elendil = new Charakter(36, "Elendil", Rasse.MENSCH, true);
        charaktere.add(elendil);

        Charakter eradan = new Charakter(37, "Eradan", Rasse.MENSCH, false);
        charaktere.add(eradan);

        Charakter erchirion = new Charakter(38, "Erchirion", Rasse.MENSCH, false);
        charaktere.add(erchirion);

        Charakter estelmo = new Charakter(39, "Estelmo", Rasse.MENSCH, false);
        charaktere.add(estelmo);

        Charakter faramir = new Charakter(40, "Faramir", Rasse.MENSCH, true);
        charaktere.add(faramir);

        Charakter findegil = new Charakter(41, "Findegil", Rasse.MENSCH, false);
        charaktere.add(findegil);

        Charakter forlong = new Charakter(42, "Forlong", Rasse.MENSCH, false);
        charaktere.add(forlong);

        Charakter gilmith = new Charakter(43, "Gilmith", Rasse.MENSCH, false);
        charaktere.add(gilmith);

        Charakter golasgil = new Charakter(44, "Golasgil", Rasse.MENSCH, false);
        charaktere.add(golasgil);

        Charakter galador = new Charakter(45, "Galador", Rasse.MENSCH, false);
        charaktere.add(galador);

        Charakter hador = new Charakter(46, "Hador", Rasse.MENSCH, false);
        charaktere.add(hador);

        Charakter hallas = new Charakter(47, "Hallas", Rasse.MENSCH, false);
        charaktere.add(hallas);

        Charakter herion = new Charakter(48, "Herion", Rasse.MENSCH, false);
        charaktere.add(herion);

        Charakter hirgon = new Charakter(49, "Hirgon", Rasse.MENSCH, false);
        charaktere.add(hirgon);

        Charakter hirluin = new Charakter(50, "Hirluin", Rasse.MENSCH, false);
        charaktere.add(hirluin);

        Charakter hurin = new Charakter(51, "Hurin", Rasse.MENSCH, false);
        charaktere.add(hurin);

        Charakter hyarmendacil = new Charakter(52, "Hyarmendacil", Rasse.MENSCH, false);
        charaktere.add(hyarmendacil);

        Charakter imrahil = new Charakter(53, "Imrahil", Rasse.MENSCH, false);
        charaktere.add(imrahil);

        Charakter imrazor = new Charakter(54, "Imrazor", Rasse.MENSCH, false);
        charaktere.add(imrazor);

        Charakter ingold = new Charakter(55, "Ingold", Rasse.MENSCH, false);
        charaktere.add(ingold);

        Charakter ioreth = new Charakter(56, "Ioreth", Rasse.MENSCH, false);
        charaktere.add(ioreth);

        Charakter iorlas = new Charakter(57, "Iorlas", Rasse.MENSCH, false);
        charaktere.add(iorlas);

        Charakter lothiriel = new Charakter(58, "Lothiriel", Rasse.MENSCH, false);
        charaktere.add(lothiriel);

        Charakter mablung = new Charakter(59, "Mablung", Rasse.MENSCH, false);
        charaktere.add(mablung);

        Charakter mardil = new Charakter(60, "Mardil", Rasse.MENSCH, false);
        charaktere.add(mardil);

        Charakter meneldil = new Charakter(61, "Meneldil", Rasse.MENSCH, false);
        charaktere.add(meneldil);

        Charakter minardil = new Charakter(62, "Minardil", Rasse.MENSCH, false);
        charaktere.add(minardil);

        Charakter minastan = new Charakter(63, "Minastan", Rasse.MENSCH, false);
        charaktere.add(minastan);

        Charakter minohtar = new Charakter(64, "Minohtar", Rasse.MENSCH, false);
        charaktere.add(minohtar);

        Charakter narmacil = new Charakter(65, "Narmacil", Rasse.MENSCH, false);
        charaktere.add(narmacil);

        Charakter ohtar = new Charakter(66, "Ohtar", Rasse.MENSCH, false);
        charaktere.add(ohtar);

        Charakter ondoher = new Charakter(67, "Ondoher", Rasse.MENSCH, false);
        charaktere.add(ondoher);

        Charakter ostoher = new Charakter(68, "Ostoher", Rasse.MENSCH, false);
        charaktere.add(ostoher);

        Charakter pelendur = new Charakter(69, "Pelendur", Rasse.MENSCH, false);
        charaktere.add(pelendur);

        Charakter rian = new Charakter(70, "Rian", Rasse.MENSCH, false);
        charaktere.add(rian);

        Charakter romendacil = new Charakter(71, "Romendacil", Rasse.MENSCH, false);
        charaktere.add(romendacil);

        Charakter siriondil = new Charakter(72, "Siriondil", Rasse.MENSCH, false);
        charaktere.add(siriondil);

        Charakter tarannon = new Charakter(73, "Tarannon", Rasse.MENSCH, false);
        charaktere.add(tarannon);

        Charakter tarciryan = new Charakter(74, "Tarciryan", Rasse.MENSCH, false);
        charaktere.add(tarciryan);

        Charakter targon = new Charakter(75, "Targon", Rasse.MENSCH, false);
        charaktere.add(targon);

        Charakter telemnar = new Charakter(76, "Telemnar", Rasse.MENSCH, false);
        charaktere.add(telemnar);

        Charakter telumehtar = new Charakter(77, "Telumehtar", Rasse.MENSCH, false);
        charaktere.add(telumehtar);

        Charakter thorondir = new Charakter(78, "Thorondir", Rasse.MENSCH, false);
        charaktere.add(thorondir);

        Charakter turambar = new Charakter(79, "Turambar", Rasse.MENSCH, false);
        charaktere.add(turambar);

        Charakter turgon = new Charakter(80, "Turgon", Rasse.MENSCH, false);
        charaktere.add(turgon);

        Charakter turin = new Charakter(81, "Turin", Rasse.MENSCH, true);
        charaktere.add(turin);

        Charakter valacar = new Charakter(82, "Valacar", Rasse.MENSCH, false);
        charaktere.add(valacar);

        Charakter vorondil = new Charakter(83, "Vorondil", Rasse.MENSCH, false);
        charaktere.add(vorondil);


        //Rohirrim


        charaktere.add(new Charakter(84, "Aldor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(85, "Baldor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(86, "Brego", Rasse.MENSCH, false));

        charaktere.add(new Charakter(87, "Brytta", Rasse.MENSCH, false));

        charaktere.add(new Charakter(88, "Ceorl", Rasse.MENSCH, false));

        charaktere.add(new Charakter(89, "Deor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(90, "Deorwine", Rasse.MENSCH, false));

        charaktere.add(new Charakter(91, "Dunhere", Rasse.MENSCH, false));

        charaktere.add(new Charakter(92, "Elfhelm", Rasse.MENSCH, false));

        charaktere.add(new Charakter(93, "Elfhild", Rasse.MENSCH, false));

        charaktere.add(new Charakter(94, "Elfwine", Rasse.MENSCH, false));

        charaktere.add(new Charakter(95, "Eofor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(96, "Eomer", Rasse.MENSCH, true));

        charaktere.add(new Charakter(97, "Eomund", Rasse.MENSCH, false));

        charaktere.add(new Charakter(98, "Eorl", Rasse.MENSCH, false));

        charaktere.add(new Charakter(99, "Eothain", Rasse.MENSCH, false));

        charaktere.add(new Charakter(100, "Eowyn", Rasse.MENSCH, true));

        charaktere.add(new Charakter(101, "Erkenbrand", Rasse.MENSCH, false));

        charaktere.add(new Charakter(102, "Fastred", Rasse.MENSCH, false));

        charaktere.add(new Charakter(103, "Fengel", Rasse.MENSCH, false));

        charaktere.add(new Charakter(104, "Folca", Rasse.MENSCH, false));

        charaktere.add(new Charakter(105, "Foldred", Rasse.MENSCH, false));

        charaktere.add(new Charakter(106, "Folcwine", Rasse.MENSCH, false));

        charaktere.add(new Charakter(107, "Frea", Rasse.MENSCH, false));

        charaktere.add(new Charakter(108, "Frealaf", Rasse.MENSCH, false));

        charaktere.add(new Charakter(109, "Freawine", Rasse.MENSCH, false));

        charaktere.add(new Charakter(110, "Galmod", Rasse.MENSCH, false));

        charaktere.add(new Charakter(111, "Gamling", Rasse.MENSCH, false));

        charaktere.add(new Charakter(112, "Garulf", Rasse.MENSCH, false));

        charaktere.add(new Charakter(113, "Gleowine", Rasse.MENSCH, false));

        charaktere.add(new Charakter(114, "Goldwine", Rasse.MENSCH, false));

        charaktere.add(new Charakter(115, "Gram", Rasse.MENSCH, false));

        charaktere.add(new Charakter(116, "Grima", Rasse.MENSCH, true));

        charaktere.add(new Charakter(117, "Grimbold", Rasse.MENSCH, false));

        charaktere.add(new Charakter(118, "Guthlaf", Rasse.MENSCH, false));

        charaktere.add(new Charakter(119, "Haleth", Rasse.MENSCH, false));

        charaktere.add(new Charakter(120, "Hama", Rasse.MENSCH, true));

        charaktere.add(new Charakter(121, "Helm", Rasse.MENSCH, false));

        charaktere.add(new Charakter(122, "Herefara", Rasse.MENSCH, false));

        charaktere.add(new Charakter(123, "Hild", Rasse.MENSCH, false));

        charaktere.add(new Charakter(124, "Horn", Rasse.MENSCH, false));

        charaktere.add(new Charakter(125, "Leod", Rasse.MENSCH, false));

        charaktere.add(new Charakter(126, "Morwen", Rasse.MENSCH, false));

        charaktere.add(new Charakter(127, "Thengel", Rasse.MENSCH, false));

        charaktere.add(new Charakter(128, "Theoden", Rasse.MENSCH, true));

        charaktere.add(new Charakter(129, "Theodred", Rasse.MENSCH, false));

        charaktere.add(new Charakter(130, "Theodwyn", Rasse.MENSCH, false));

        charaktere.add(new Charakter(131, "Walda", Rasse.MENSCH, false));

        charaktere.add(new Charakter(132, "Widfara", Rasse.MENSCH, false));


        //Dunedain


        charaktere.add(new Charakter(133, "Amlaith", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(134, "Arador", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(135, "Araglas", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(136, "Aragorn", Rasse.DUNEDAIN, true));

        charaktere.add(new Charakter(137, "Aragost", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(138, "Arahad", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(139, "Arahael", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(140, "Aranarth", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(141, "Arantar", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(142, "Aranuir", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(143, "Araphant", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(144, "Araphor", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(145, "Arassuil", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(146, "Arathorn", Rasse.DUNEDAIN, true));

        charaktere.add(new Charakter(147, "Araval", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(148, "Aravir", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(149, "Aravorn", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(150, "Argeleb", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(151, "Argonui", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(152, "Arvedui", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(153, "Arvegil", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(154, "Arveleg", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(155, "Celebrindor", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(156, "Celepharn", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(157, "Dirhael", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(158, "Earendur", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(159, "Eldacar", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(160, "Eldarion", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(161, "Elendur", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(162, "Gilraen", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(163, "Halbarad", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(164, "Isildur", Rasse.DUNEDAIN, true));

        charaktere.add(new Charakter(165, "Malbeth", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(166, "Mallor", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(167, "Malvegil", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(168, "Tarcil", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(169, "Tarondor", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(170, "Valandil", Rasse.DUNEDAIN, false));

        charaktere.add(new Charakter(171, "Valandur", Rasse.DUNEDAIN, false));


        //Numeronerer


        charaktere.add(new Charakter(172, "Alinel", Rasse.MENSCH, false));

        charaktere.add(new Charakter(173, "Almarian", Rasse.MENSCH, false));

        charaktere.add(new Charakter(174, "Almiel", Rasse.MENSCH, false));

        charaktere.add(new Charakter(175, "Amandil", Rasse.MENSCH, false));

        charaktere.add(new Charakter(176, "Anarion", Rasse.MENSCH, false));

        charaktere.add(new Charakter(177, "Ar-Adunakhor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(178, "Ar-Gimilzor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(179, "Ar-Pharazon", Rasse.MENSCH, false));

        charaktere.add(new Charakter(180, "Ar-Sakalthor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(181, "Ar-Zimrathon", Rasse.MENSCH, false));

        charaktere.add(new Charakter(182, "Beregar", Rasse.MENSCH, false));

        charaktere.add(new Charakter(183, "Ciryatur", Rasse.MENSCH, false));

        charaktere.add(new Charakter(184, "Elatan", Rasse.MENSCH, false));

        charaktere.add(new Charakter(185, "Elros", Rasse.MENSCH, false));

        charaktere.add(new Charakter(186, "Erendis", Rasse.MENSCH, false));

        charaktere.add(new Charakter(187, "Fuinur", Rasse.MENSCH, false));

        charaktere.add(new Charakter(188, "Gimilkhad", Rasse.MENSCH, false));

        charaktere.add(new Charakter(189, "Gimilzagar", Rasse.MENSCH, false));

        charaktere.add(new Charakter(190, "Hallacar", Rasse.MENSCH, false));

        charaktere.add(new Charakter(191, "Hatholdir", Rasse.MENSCH, false));

        charaktere.add(new Charakter(192, "Hunderch", Rasse.MENSCH, false));

        charaktere.add(new Charakter(193, "Herucalmo", Rasse.MENSCH, false));

        charaktere.add(new Charakter(194, "Herumor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(195, "Ibal", Rasse.MENSCH, false));

        charaktere.add(new Charakter(196, "Inzelbeth", Rasse.MENSCH, false));

        charaktere.add(new Charakter(197, "Isilmo", Rasse.MENSCH, false));

        charaktere.add(new Charakter(198, "Lindorie", Rasse.MENSCH, false));

        charaktere.add(new Charakter(199, "Malantur", Rasse.MENSCH, false));

        charaktere.add(new Charakter(200, "Numendil", Rasse.MENSCH, false));

        charaktere.add(new Charakter(201, "Nuneth", Rasse.MENSCH, false));

        charaktere.add(new Charakter(202, "Orchaldor", Rasse.MENSCH, false));

        charaktere.add(new Charakter(203, "Silmarien", Rasse.MENSCH, false));

        charaktere.add(new Charakter(204, "Soronto", Rasse.MENSCH, false));

        charaktere.add(new Charakter(205, "Tar-Alcarin", Rasse.MENSCH, false));

        charaktere.add(new Charakter(206, "Tar-Aldarion", Rasse.MENSCH, false));

        charaktere.add(new Charakter(207, "Tar-Ancalime", Rasse.MENSCH, false));

        charaktere.add(new Charakter(208, "Tar-Ancalimon", Rasse.MENSCH, false));

        charaktere.add(new Charakter(209, "Tar-Ardamin", Rasse.MENSCH, false));

        charaktere.add(new Charakter(210, "Tar-Atanamir", Rasse.MENSCH, false));

        charaktere.add(new Charakter(211, "Tar-Calion", Rasse.MENSCH, false));

        charaktere.add(new Charakter(212, "Tar-Calmacil", Rasse.MENSCH, false));

        charaktere.add(new Charakter(213, "Tar-Ciryatan", Rasse.MENSCH, false));

        charaktere.add(new Charakter(214, "Tar-Elendil", Rasse.MENSCH, false));

        charaktere.add(new Charakter(215, "Tar-Meneldur", Rasse.MENSCH, false));

        charaktere.add(new Charakter(216, "Tar-Miriel", Rasse.MENSCH, false));

        charaktere.add(new Charakter(217, "Tar-Surion", Rasse.MENSCH, false));

        charaktere.add(new Charakter(218, "Tar-Telemmaite", Rasse.MENSCH, false));

        charaktere.add(new Charakter(219, "Tar-Telperien", Rasse.MENSCH, false));

        charaktere.add(new Charakter(220, "Tar-Vanimelde", Rasse.MENSCH, false));

        charaktere.add(new Charakter(221, "Ulbar", Rasse.MENSCH, false));

        charaktere.add(new Charakter(222, "Vardamir", Rasse.MENSCH, false));

        charaktere.add(new Charakter(223, "Veantur", Rasse.MENSCH, false));

        charaktere.add(new Charakter(224, "Zamin", Rasse.MENSCH, false));


        //Ostlinge

        charaktere.add(new Charakter(225, "Bor", Rasse.OSTLING, false));

        charaktere.add(new Charakter(226, "Borlach", Rasse.OSTLING, false));

        charaktere.add(new Charakter(227, "Borlad", Rasse.OSTLING, false));

        charaktere.add(new Charakter(228, "Borthand", Rasse.OSTLING, false));

        charaktere.add(new Charakter(229, "Brodda", Rasse.OSTLING, false));

        charaktere.add(new Charakter(230, "Lorgan", Rasse.OSTLING, false));

        charaktere.add(new Charakter(231, "Uldor", Rasse.OSTLING, false));

        charaktere.add(new Charakter(232, "Ulfang", Rasse.OSTLING, false));

        charaktere.add(new Charakter(233, "Ulfast", Rasse.OSTLING, false));

        charaktere.add(new Charakter(234, "Ulwarth", Rasse.OSTLING, false));


        //Elben


        charaktere.add(new Charakter(235, "Aegnor", Rasse.ELB, false));

        charaktere.add(new Charakter(236, "Ainairos", Rasse.ELB, false));

        charaktere.add(new Charakter(237, "Amarie", Rasse.ELB, false));

        charaktere.add(new Charakter(238, "Amdir", Rasse.ELB, false));

        charaktere.add(new Charakter(239, "Amras", Rasse.ELB, false));

        charaktere.add(new Charakter(240, "Amrod", Rasse.ELB, false));

        charaktere.add(new Charakter(241, "Amroth", Rasse.ELB, false));

        charaktere.add(new Charakter(242, "Angrod", Rasse.ELB, false));

        charaktere.add(new Charakter(243, "Annael", Rasse.ELB, false));

        charaktere.add(new Charakter(244, "Aranwe", Rasse.ELB, false));

        charaktere.add(new Charakter(245, "Aredhel", Rasse.ELB, false));

        charaktere.add(new Charakter(246, "Arminas", Rasse.ELB, false));

        charaktere.add(new Charakter(247, "Arwen", Rasse.ELB, true));

        charaktere.add(new Charakter(248, "Beleg", Rasse.ELB, false));

        charaktere.add(new Charakter(249, "Bruithwir", Rasse.ELB, false));

        charaktere.add(new Charakter(250, "Caranthir", Rasse.ELB, false));

        //Ab hier anderer Konstruktor

        charaktere.add(new Charakter(251, "Celeborn", Rasse.ELB, true));

        charaktere.add(new Charakter(252, "Clebrian", Rasse.ELB, false));

        charaktere.add(new Charakter(253, "Celebrimbor", Rasse.ELB, true));

        charaktere.add(new Charakter(254, "Celegorm", Rasse.ELB, false));

        charaktere.add(new Charakter(255, "Cirdan", Rasse.ELB, false));

        charaktere.add(new Charakter(256, "Curufin", Rasse.ELB, false));

        charaktere.add(new Charakter(257, "Daeron", Rasse.ELB, false));

        charaktere.add(new Charakter(258, "Daurin", Rasse.ELB, false));

        charaktere.add(new Charakter(259, "Dior", Rasse.ELB, false));

        charaktere.add(new Charakter(260, "Earendil", Rasse.ELB, false));

        charaktere.add(new Charakter(261, "Earwen", Rasse.ELB, false));

        charaktere.add(new Charakter(262, "Ecthelion", Rasse.ELB, false));

        charaktere.add(new Charakter(263, "Edrahil", Rasse.ELB, false));

        charaktere.add(new Charakter(264, "Egnor", Rasse.ELB, false));

        charaktere.add(new Charakter(265, "Elemmakill", Rasse.ELB, false));

        charaktere.add(new Charakter(266, "Elemmire", Rasse.ELB, false));

        charaktere.add(new Charakter(267, "Elenwe", Rasse.ELB, false));

        charaktere.add(new Charakter(268, "Elladan", Rasse.ELB, false));

        charaktere.add(new Charakter(269, "Elmo", Rasse.ELB, false));

        charaktere.add(new Charakter(270, "Elrohir", Rasse.ELB, false));

        charaktere.add(new Charakter(271, "Elrond", Rasse.ELB, true));

        charaktere.add(new Charakter(272, "Eltas", Rasse.ELB, false));

        charaktere.add(new Charakter(273, "Elured", Rasse.ELB, false));

        charaktere.add(new Charakter(274, "Elurin", Rasse.ELB, false));

        charaktere.add(new Charakter(275, "Elwe", Rasse.ELB, false));

        charaktere.add(new Charakter(276, "Elwing", Rasse.ELB, false));

        charaktere.add(new Charakter(277, "Enel", Rasse.ELB, false));

        charaktere.add(new Charakter(278, "Enelye", Rasse.ELB, false));

        charaktere.add(new Charakter(279, "Enerdhil", Rasse.ELB, false));

        charaktere.add(new Charakter(280, "Eol", Rasse.ELB, false));

        charaktere.add(new Charakter(281, "Evromord", Rasse.ELB, false));

        charaktere.add(new Charakter(282, "Ereinion", Rasse.ELB, false));

        charaktere.add(new Charakter(283, "Erestor", Rasse.ELB, false));

        charaktere.add(new Charakter(284, "Feanor", Rasse.ELB, true));

        charaktere.add(new Charakter(285, "Finarfin", Rasse.ELB, false));

        charaktere.add(new Charakter(286, "Finduilas", Rasse.ELB, false));

        charaktere.add(new Charakter(287, "Fingolfin", Rasse.ELB, true));

        charaktere.add(new Charakter(288, "Fingon", Rasse.ELB, false));

        charaktere.add(new Charakter(289, "Finrod", Rasse.ELB, false));

        charaktere.add(new Charakter(290, "Finwe", Rasse.ELB, true));

        charaktere.add(new Charakter(291, "Galadhon", Rasse.ELB, false));

        charaktere.add(new Charakter(292, "Galadriel", Rasse.ELB, true));

        charaktere.add(new Charakter(293, "Galathil", Rasse.ELB, false));

        charaktere.add(new Charakter(294, "Galdor", Rasse.ELB, false));

        charaktere.add(new Charakter(295, "Gelmir", Rasse.ELB, false));

        charaktere.add(new Charakter(296, "Gildor", Rasse.ELB, false));

        charaktere.add(new Charakter(297, "Gilfanon", Rasse.ELB, false));

        charaktere.add(new Charakter(298, "Gil-Galad", Rasse.ELB, true));

        charaktere.add(new Charakter(299, "Glorfindel", Rasse.ELB, false));

        charaktere.add(new Charakter(300, "Guilin", Rasse.ELB, false));

        charaktere.add(new Charakter(301, "Gwindor", Rasse.ELB, false));

        charaktere.add(new Charakter(302, "Haldir", Rasse.ELB, true));

        charaktere.add(new Charakter(303, "Idril", Rasse.ELB, false));

        charaktere.add(new Charakter(304, "Ilverin", Rasse.ELB, false));

        charaktere.add(new Charakter(305, "Indis", Rasse.ELB, false));

        charaktere.add(new Charakter(306, "Ingwe", Rasse.ELB, false));

        charaktere.add(new Charakter(307, "Imin", Rasse.ELB, false));

        charaktere.add(new Charakter(308, "Iminye", Rasse.ELB, false));

        charaktere.add(new Charakter(309, "Ingil", Rasse.ELB, false));

        charaktere.add(new Charakter(310, "Ithilbor", Rasse.ELB, false));

        charaktere.add(new Charakter(311, "Legolas", Rasse.ELB, true));

        charaktere.add(new Charakter(312, "Lenwe", Rasse.ELB, false));

        charaktere.add(new Charakter(313, "Lindir", Rasse.ELB, false));

        charaktere.add(new Charakter(314, "Lindo", Rasse.ELB, false));

        charaktere.add(new Charakter(315, "Luthien", Rasse.ELB, false));

        charaktere.add(new Charakter(316, "Mablon", Rasse.ELB, false));

        charaktere.add(new Charakter(317, "Maedhros", Rasse.ELB, false));

        charaktere.add(new Charakter(318, "Maeglin", Rasse.ELB, false));

        charaktere.add(new Charakter(319, "Maglor", Rasse.ELB, false));

        charaktere.add(new Charakter(320, "Mahtan", Rasse.ELB, false));

        charaktere.add(new Charakter(321, "Maidros", Rasse.ELB, false));

        charaktere.add(new Charakter(322, "Meril", Rasse.ELB, false));

        charaktere.add(new Charakter(323, "Miriel", Rasse.ELB, false));

        charaktere.add(new Charakter(324, "Mithrellas", Rasse.ELB, false));

        charaktere.add(new Charakter(325, "Nellas", Rasse.ELB, false));

        charaktere.add(new Charakter(326, "Nerdanel", Rasse.ELB, false));

        charaktere.add(new Charakter(327, "Nimloth", Rasse.ELB, false));

        charaktere.add(new Charakter(328, "Nimrodel", Rasse.ELB, false));

        charaktere.add(new Charakter(329, "Nuin", Rasse.ELB, false));

        charaktere.add(new Charakter(330, "Olwe", Rasse.ELB, false));

        charaktere.add(new Charakter(331, "Orodreth", Rasse.ELB, false));

        charaktere.add(new Charakter(332, "Orophin", Rasse.ELB, false));

        charaktere.add(new Charakter(333, "Oropher", Rasse.ELB, false));

        charaktere.add(new Charakter(334, "Rumil", Rasse.ELB, false));

        charaktere.add(new Charakter(335, "Saeros", Rasse.ELB, false));

        charaktere.add(new Charakter(336, "Tareg", Rasse.ELB, false));

        charaktere.add(new Charakter(337, "Tata", Rasse.ELB, false));

        charaktere.add(new Charakter(338, "Tatie", Rasse.ELB, false));

        charaktere.add(new Charakter(339, "Tauriel", Rasse.ELB, true));

        charaktere.add(new Charakter(340, "Thingol", Rasse.ELB, false));

        charaktere.add(new Charakter(341, "Thranduil", Rasse.ELB, false));

        charaktere.add(new Charakter(342, "Tinfang", Rasse.ELB, false));

        charaktere.add(new Charakter(343, "Tulkastor", Rasse.ELB, false));

        charaktere.add(new Charakter(344, "Tuvo", Rasse.ELB, false));

        charaktere.add(new Charakter(345, "Uole", Rasse.ELB, false));

        charaktere.add(new Charakter(346, "Vaire", Rasse.ELB, false));

        charaktere.add(new Charakter(347, "Voronwe", Rasse.ELB, false));



        //Zwerge


        charaktere.add(new Charakter(348, "Azaghal", Rasse.ZWERG, false));

        charaktere.add(new Charakter(349, "Balin", Rasse.ZWERG, false));

        charaktere.add(new Charakter(350, "Bifur", Rasse.ZWERG, true));

        charaktere.add(new Charakter(351, "Bofur", Rasse.ZWERG, true));

        charaktere.add(new Charakter(352, "Bombur", Rasse.ZWERG, false));

        charaktere.add(new Charakter(353, "Borin", Rasse.ZWERG, false));

        charaktere.add(new Charakter(354, "Dain", Rasse.ZWERG, false));

        charaktere.add(new Charakter(355, "Dis", Rasse.ZWERG, false));

        charaktere.add(new Charakter(356, "Dori", Rasse.ZWERG, false));

        charaktere.add(new Charakter(357, "Durin", Rasse.ZWERG, true));

        charaktere.add(new Charakter(358, "Dwalin", Rasse.ZWERG, true));

        charaktere.add(new Charakter(359, "Farin", Rasse.ZWERG, false));

        charaktere.add(new Charakter(360, "Fili", Rasse.ZWERG, false));

        charaktere.add(new Charakter(361, "Floi", Rasse.ZWERG, false));

        charaktere.add(new Charakter(362, "Frar", Rasse.ZWERG, false));

        charaktere.add(new Charakter(363, "Frerin", Rasse.ZWERG, false));

        charaktere.add(new Charakter(364, "Fror", Rasse.ZWERG, false));

        charaktere.add(new Charakter(365, "Fundin", Rasse.ZWERG, false));

        charaktere.add(new Charakter(366, "Gamli", Rasse.ZWERG, false));

        charaktere.add(new Charakter(367, "Gimli", Rasse.ZWERG, true));

        charaktere.add(new Charakter(368, "Gloin", Rasse.ZWERG, true));

        charaktere.add(new Charakter(369, "Groin", Rasse.ZWERG, false));

        charaktere.add(new Charakter(370, "Gror", Rasse.ZWERG, false));

        charaktere.add(new Charakter(371, "Ibun", Rasse.ZWERG, false));

        charaktere.add(new Charakter(372, "Khim", Rasse.ZWERG, false));

        charaktere.add(new Charakter(373, "Kili", Rasse.ZWERG, true));

        charaktere.add(new Charakter(374, "Loni", Rasse.ZWERG, false));

        charaktere.add(new Charakter(375, "Mim", Rasse.ZWERG, false));

        charaktere.add(new Charakter(376, "Nain", Rasse.ZWERG, false));

        charaktere.add(new Charakter(377, "Nali", Rasse.ZWERG, false));

        charaktere.add(new Charakter(378, "Narvi", Rasse.ZWERG, false));

        charaktere.add(new Charakter(379, "Nori", Rasse.ZWERG, false));

        charaktere.add(new Charakter(380, "Oin", Rasse.ZWERG, false));

        charaktere.add(new Charakter(381, "Ori", Rasse.ZWERG, false));

        charaktere.add(new Charakter(382, "Telchar", Rasse.ZWERG, false));

        charaktere.add(new Charakter(383, "Thorin", Rasse.ZWERG, true));

        charaktere.add(new Charakter(384, "Thrain", Rasse.ZWERG, true));

        charaktere.add(new Charakter(385, "Thror", Rasse.ZWERG, true));


        //Hobbits


        charaktere.add(new Charakter(386, "Adaldrida", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(387, "Adalgrim", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(388, "Adamanta", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(389, "Adelard", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(390, "Amaranth", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(391, "Andweis", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(392, "Angelica", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(393, "Ansen", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(394, "Asphodel", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(395, "Bandobras", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(396, "Balbo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(397, "Belba", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(398, "Bell", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(399, "Belladonna", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(400, "Berilac", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(401, "Berylla", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(402, "Bilbo", Rasse.HOBBIT, true));

        charaktere.add(new Charakter(403, "Bingo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(404, "Blanco", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(405, "Bodo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(406, "Bogenmann", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(407, "Bucca", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(408, "Bungo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(409, "Camellia", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(410, "Carl", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(411, "Celandine", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(412, "Chica", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(413, "Deagol", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(414, "Dietmute", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(415, "Dinodas", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(416, "Doderic", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(417, "Dodinas", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(418, "Donnamira", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(419, "Dora", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(420, "Drogo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(421, "Dudo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(422, "Elanor", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(423, "Erling", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(424, "Esmeralda", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(425, "Estella", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(426, "Everad", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(427, "Falco", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(428, "Fastolph", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(429, "Ferdibrand", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(430, "Ferdinand", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(431, "Ferumbas", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(432, "Filibert", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(433, "Flambart", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(434, "Folko", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(435, "Fortinbras", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(436, "Fredegar", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(437, "Frodo", Rasse.HOBBIT, true));

        charaktere.add(new Charakter(438, "Gerontius", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(439, "Goldblume", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(440, "Goldlöckchen", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(441, "Gorbadoc", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(442, "Gorbulas", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(443, "Gorhendad", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(444, "Gormadoc", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(445, "Griffo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(446, "Gundabald", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(447, "Halfast", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(448, "Halfred", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(449, "Hamfast", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(450, "Hamsen", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(451, "Hanna", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(452, "Harding", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(453, "Heiderose", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(454, "Hending", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(455, "Hilda", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(456, "Hildibrand", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(457, "Hildifons", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(458, "Hildigard", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(459, "Hildigrim", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(460, "Hob", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(461, "Hobsen", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(462, "Holfast", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(463, "Holman", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(464, "Hugo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(465, "Ilberic", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(466, "Isegrim", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(467, "Isembard", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(468, "Isembold", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(469, "Isengar", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(470, "Kunz", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(471, "Labkraut", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(472, "Largo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(473, "Laura", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(474, "Lily", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(475, "Linda", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(476, "Lobelia", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(477, "Longo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(478, "Lotho", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(479, "Madoc", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(480, "Maggot", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(481, "Magsame", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(482, "Maie", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(483, "Malva", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(484, "Marcho", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(485, "Margerite", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(486, "Marmadas", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(487, "Marroc", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(488, "Melilot", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(489, "Menegilda", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(490, "Merry", Rasse.HOBBIT, true));

        charaktere.add(new Charakter(491, "Merimac", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(492, "Merimas", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(493, "Milo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(494, "Minto", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(495, "Minze", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(496, "Mirabella", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(497, "Mosco", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(498, "Mungo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(499, "Odo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(500, "Odovacar", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(501, "Otho", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(502, "Peregrin", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(503, "Petuina", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(504, "Pippin", Rasse.HOBBIT, true));

        charaktere.add(new Charakter(505, "Polo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(506, "Ponto", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(507, "Posco", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(508, "Primula", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(509, "Reginard", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(510, "Salvia", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(511, "Seredic", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(512, "Sam", Rasse.HOBBIT, true));

        charaktere.add(new Charakter(513, "Smeagol", Rasse.HOBBIT, true));

        charaktere.add(new Charakter(514, "Tobold", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(515, "Togo", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(516, "Viala", Rasse.HOBBIT, false));

        charaktere.add(new Charakter(517, "Willi", Rasse.HOBBIT, false));

        //Magier


        charaktere.add(new Charakter(518, "Alatar", Rasse.MAGIER, false));

        charaktere.add(new Charakter(519, "Gandalf", Rasse.MAGIER, true));

        charaktere.add(new Charakter(520, "Pallando", Rasse.MAGIER, false));

        charaktere.add(new Charakter(521, "Radagast", Rasse.MAGIER, false));

        charaktere.add(new Charakter(522, "Saruman", Rasse.MAGIER, true));

        charaktere.add(new Charakter(523, "Tarais", Rasse.MAGIER, false));

        charaktere.add(new Charakter(524, "Diokis", Rasse.MAGIER, false));

        charaktere.add(new Charakter(525, "Maloorn", Rasse.MAGIER, false));

        charaktere.add(new Charakter(526, "Meanel", Rasse.MAGIER, false));

        charaktere.add(new Charakter(527, "Faenad", Rasse.MAGIER, false));

        charaktere.add(new Charakter(528, "Garano", Rasse.MAGIER, false));


        //Ents


        charaktere.add(new Charakter(529, "Bregalad", Rasse.ENT, false));

        charaktere.add(new Charakter(530, "Buchenbein", Rasse.ENT, false));

        charaktere.add(new Charakter(531, "Baumbart", Rasse.ENT, true));

        charaktere.add(new Charakter(532, "Fimbrethil", Rasse.ENT, false));

        charaktere.add(new Charakter(533, "Finglas", Rasse.ENT, false));

        charaktere.add(new Charakter(534, "Fladrif", Rasse.ENT, false));


        //Orks


        charaktere.add(new Charakter(535, "Azog", Rasse.URUKHAI, true));

        charaktere.add(new Charakter(536, "Lurtz", Rasse.URUKHAI, true));

        charaktere.add(new Charakter(537, "Bolg", Rasse.URUKHAI, false));

        charaktere.add(new Charakter(538, "Murgash", Rasse.URUKHAI, false));

        charaktere.add(new Charakter(539, "Gorbag", Rasse.URUKHAI, false));



        charaktere.add(new Charakter(540, "Grischnakh", Rasse.ORK, true));

        charaktere.add(new Charakter(541, "Lagduf", Rasse.ORK, false));

        charaktere.add(new Charakter(542, "Lugdusch", Rasse.ORK, false));

        charaktere.add(new Charakter(543, "Mauhur", Rasse.ORK, true));

        charaktere.add(new Charakter(544, "Muzgasch", Rasse.ORK, false));

        charaktere.add(new Charakter(545, "Radbug", Rasse.ORK, false));

        charaktere.add(new Charakter(546, "Schagrat", Rasse.ORK, false));

        charaktere.add(new Charakter(547, "Ufthak", Rasse.ORK, false));

        charaktere.add(new Charakter(548, "Ugluk", Rasse.ORK, true));

        charaktere.add(new Charakter(549, "Golblog", Rasse.ORK, false));

        charaktere.add(new Charakter(550, "Aschrog", Rasse.ORK, false));

        charaktere.add(new Charakter(551, "Lopogh", Rasse.ORK, false));

        charaktere.add(new Charakter(552, "Golpur", Rasse.ORK, false));

        charaktere.add(new Charakter(553, "Rokosch", Rasse.ORK, false));

        charaktere.add(new Charakter(554, "Borrok", Rasse.ORK, false));

        charaktere.add(new Charakter(555, "Murlk", Rasse.ORK, false));

        charaktere.add(new Charakter(556, "Bogom", Rasse.ORK, false));

        charaktere.add(new Charakter(557, "Rokuh", Rasse.ORK, false));

        charaktere.add(new Charakter(558, "Lopgolbor", Rasse.ORK, false));

        charaktere.add(new Charakter(559, "Roukolk", Rasse.ORK, false));

        charaktere.add(new Charakter(560, "Purrokasch", Rasse.ORK, false));

        charaktere.add(new Charakter(561, "Wyolk", Rasse.ORK, false));

        charaktere.add(new Charakter(562, "Rogtarkolg", Rasse.ORK, false));

        charaktere.add(new Charakter(563, "Rakosch", Rasse.ORK, false));

        charaktere.add(new Charakter(564, "Raktok", Rasse.ORK, false));

        charaktere.add(new Charakter(565, "Morbrabor", Rasse.ORK, false));

        charaktere.add(new Charakter(566, "Rakvoc", Rasse.ORK, false));

        charaktere.add(new Charakter(567, "Drarog", Rasse.ORK, false));

        charaktere.add(new Charakter(568, "Tarkrog", Rasse.ORK, false));

        charaktere.add(new Charakter(569, "Turgh", Rasse.ORK, false));

        charaktere.add(new Charakter(570, "Purrouk", Rasse.ORK, false));

        charaktere.add(new Charakter(571, "Rogtok", Rasse.ORK, false));

        charaktere.add(new Charakter(572, "Roukox", Rasse.ORK, false));

        charaktere.add(new Charakter(573, "Golm", Rasse.ORK, false));

        charaktere.add(new Charakter(574, "Murlolg", Rasse.ORK, false));

        //Nazgulu

        charaktere.add(new Charakter(575, "Murazor", Rasse.NAZGUL, true));

        charaktere.add(new Charakter(576, "Khamul", Rasse.NAZGUL, true));

        charaktere.add(new Charakter(577, "Darogh", Rasse.NAZGUL, false));

        charaktere.add(new Charakter(578, "Tarkem", Rasse.NAZGUL, false));
		
		for(Charakter c : charaktere) {
			if(ArdaCraftAPI.getACDatabase().getCharacterManager().get(c.getName()) == null) {
				ArdaCraftAPI.getACDatabase().getCharacterManager().add(c);
				System.err.println("[CHARAKTER] Character " + c.getName() + " added to Database");
			}
		}
		System.out.println("[CHARAKTER] finished character initialisation");
		
		
	}
	
}
