package me.tobi.ardacraft.registration.generator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GeneratorOrk {
	
	Random rnd = new Random();
	List<Character> vokale = Arrays.asList('a', 'i', 'o', 'u');
	List<Character> konsonanten = Arrays.asList('g', 'z', 'l', 'k', 'p', 'h', 'd', 'r', 't');
	
	public String getBeginning() {
		List<String> starts = Arrays.asList("Gr", "L", "M", "R", "Sch", "G", "B", "R", "P");
		return starts.get(rnd.nextInt(starts.size()));
	}
	
	public String getEnding() {
		List<String> ends = Arrays.asList("nakh", "duf", "bug", "rat", "lk", "tog", "tok", "rog", "rok", "dog", "dok", "k", "g");
		return ends.get(rnd.nextInt(ends.size()));
	}
	
	public String getMiddle() {
		char first = vokale.get(rnd.nextInt(vokale.size()));
		char second = konsonanten.get(rnd.nextInt(konsonanten.size()));
		if(rnd.nextInt(3) == 1) {
		char third = vokale.get(rnd.nextInt(vokale.size()));
		return "" + first + second + third;
		}
		return "" + first + second;
	}
	
	public String generate() {
		return getBeginning() + getMiddle() + getEnding();
	}
	
	
	
}
