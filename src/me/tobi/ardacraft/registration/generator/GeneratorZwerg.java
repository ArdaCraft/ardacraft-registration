package me.tobi.ardacraft.registration.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class GeneratorZwerg {
	
	HashMap<String, List<String>> endings = new HashMap<String, List<String>>();
	List<String> beginnings = new ArrayList<String>(); //rV, wV, Vm, Vr, VV
	List<Character> vokale = Arrays.asList('a', 'e', 'i', 'o', 'u');
	List<Character> bKonsonanten = Arrays.asList('b', 'd', 'f', 'g', 'k', 'n', 't', 'p');
	
	public void init() {
		//put a K befor each
		endings.put("rV", Arrays.asList("ur", "in", "li", "i"));
		endings.put("V", Arrays.asList("ur", "in", "li", "i"));
		endings.put("wV", Arrays.asList("in", "li"));
		endings.put("Vm", Arrays.asList("ur", "in"));
		endings.put("Vr", Arrays.asList("ur", "in", "li", "i"));
		for(String s : endings.keySet()) {
			beginnings.add(s);
		}
	}
	
	public String generate() {
		init();
		Random rnd = new Random();
		int iSChar =  rnd.nextInt(bKonsonanten.size());
		String startChar = "" + bKonsonanten.get(iSChar);      //use
		startChar = startChar.toUpperCase();
		int iBStr = rnd.nextInt(beginnings.size());
		char[] begin = beginnings.get(iBStr).toCharArray();
		//replace
		int i = 0;
		for(char c : begin) {
			if(c == 'V') {
				int v = rnd.nextInt(vokale.size());
				begin[i] = vokale.get(v);
				//begin.replaceFirst("V", "" + vokale.get(v));
			}
			i++;
		}
		//end replace
		
		if(startChar.equalsIgnoreCase("t")) {
			startChar = "Th";
		}
		
		List<String> ends = endings.get(beginnings.get(iBStr));
		int iEStr = rnd.nextInt(ends.size());
		String end = ends.get(iEStr);
		if(end == "li") {
			if(rnd.nextInt(2) == 1) {
				end = end + "n";
			}
		}
		
		String name = new String(startChar + new String(begin) + end);
		
		return name;
	}
	
}
